# Obj renderer (executable)

This project provides an executable for creating simple turntable wireframe rendering from obj files.

To do so, it uses basic implementations of some interesting entities such as:
- An OBJ files reader/parser
- 2D shapes
- 3D shapes
- Color
- Drawing brushes

### Turntable example
![](turntable_examples/roller_skate.gif)

***

For more info: www.jaimervq.com
